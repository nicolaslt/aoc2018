use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::env::args;

mod d01;
mod d02;

fn call_solver(puzzle: &str, s: &str) -> String {
  match puzzle {
    "01a" => return d01::a(s),
    "01b" => return d01::b(s),
    "02a" => return d02::a(s),
    _ => return "No solution for this puzzle yet".to_string(),
  }
}

fn main() {
	let args: Vec<String> = args().collect();

	if args.len() < 2 {
		println!("Usage: cargo run puzzleIndex input.txt");
    println!("Example: cargo run 02b input.txt");
    return
  }

  let puzzle = &args[1].to_string();
	let path = Path::new(&args[2]);
	let display = path.display();

	// Open the path in read-only mode, returns `io::Result<File>`
	let mut file = match File::open(&path) {
		// The `description` method of `io::Error` returns a string that
		// describes the error
		Err(why) => panic!("couldn't open {}: {}", display,
				why.description()),
			Ok(file) => file,
	};

	// Read the file contents into a string, returns `io::Result<usize>`
	let mut s = String::new();
	match file.read_to_string(&mut s) {
		Err(why) => panic!("couldn't read {}: {}", display,
				why.description()),
			Ok(_) => println!("{}", call_solver(&puzzle, &s)),
	}
}
