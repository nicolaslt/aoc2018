// TODO str.Lines iterator instead of s.plit; and Vec.map 
use std::collections::HashMap;

type Buckets = (u8, u8);

fn to_buckets(s: &str) -> Buckets {
  let mut buckets: HashMap<char, u8> = HashMap::new();
  for c in s.chars() {
    let count = buckets.entry(c).or_insert(0);
    *count += 1;
  }
  let mut seen2 = false;
  let mut seen3 = false;
  for val in buckets.values() {
    match val {
      3 => seen3 = true,
      2 => seen2 = true,
      _ => {}
    }
  }
  return ( seen2 as u8, seen3 as u8);
}

fn checksum(buckets: &Vec<Buckets>) -> u32 {
  let mut bucket_sum: Buckets = (0,0);
  for bucket in buckets.into_iter() {
    bucket_sum.0 += bucket.0;
    bucket_sum.1 += bucket.1;
  }
  return bucket_sum.0 as u32 * bucket_sum.1 as u32;
}

pub fn a(s: &str) -> String {
	let buckets: Vec<Buckets> = s
    .lines()
    .map(|s| to_buckets(s))
    .collect();
  return checksum(&buckets).to_string();
}

#[cfg(test)]
mod a_tests {
  use super::*;

  #[test]
  fn test_a() {
    let res: String = a("abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab");
    assert_eq!(res, "12");
  }
}

#[cfg(test)]
mod bucket_tests {
  use super::*;

  #[test]
  fn first() {
    let res: Buckets = to_buckets("abcdef");
    assert_eq!(res, (0, 0));
  }

  #[test]
  fn second() {
    let res: Buckets = to_buckets("bababc");
    assert_eq!(res, (1, 1));
  }

  #[test]
  fn third() {
    let res: Buckets = to_buckets("abbcde");
    assert_eq!(res, (1, 0));
  }

  #[test]
  fn fourth() {
    let res: Buckets = to_buckets("abcccd");
    assert_eq!(res, (0, 1));
  }

  #[test]
  fn fifth() {
    let res: Buckets = to_buckets("aabcdd");
    assert_eq!(res, (1, 0));
  }

  #[test]
  fn sixth() {
    let res: Buckets = to_buckets("abcdee");
    assert_eq!(res, (1, 0));
  }

  #[test]
  fn seventh() {
    let res: Buckets = to_buckets("ababab");
    assert_eq!(res, (0, 1));
  }
}

#[cfg(test)]
mod checksum {
  use super::*;

  #[test]
  fn makes_checksum() {
    let buckets: Vec<Buckets> = vec![(1,1), (2,0), (2,3)];
    let res: u32 = checksum(&buckets);
    assert_eq!(res, 20);
  }
}
