// TODO str.Lines iterator instead of s.plit; and Vec.map 
pub fn a(s: &str) -> String {
	let operations: Vec<&str> = s.split("\n").collect();
	let mut result: i32 = 0;
	for operation in operations.iter() {
		match i32::from_str_radix(operation, 10) {
			Err(why) => println!("{}: '{}'", why, operation),
			Ok(number) => result += number,
		}
	}
  return result.to_string();
}

#[cfg(test)]
mod a_tests {
  use super::*;

  #[test]
  fn first_sum() {
    let res: String = a("+1\n+1\n+1");
    assert_eq!(res, "3");
  }

  #[test]
  fn second_sum() {
    let res: String = a("+1\n+1\n-2");
    assert_eq!(res, "0");
  }

  #[test]
  fn third_sum() {
    let res: String = a("-1\n-2\n-3");
    assert_eq!(res, "-6");
  }
}

pub fn b (s: &str) -> String {
	let raw_operations: Vec<&str> = s.split("\n").collect();

  let mut operations: Vec<i32> = Vec::new();
  for operation in raw_operations.iter() {
    match i32::from_str_radix(operation, 10) {
      Err(why) => println!("{}: '{}'", why, operation),
      Ok(number) => operations.push(number),
    }
  }

  let mut witnessed_sums: Vec<i32> = vec![0];
	let mut result: i32 = 0;
  loop {
    println!("loop: {}", result);
    for operation in operations.iter() {
      result += operation;
      if witnessed_sums.contains(&result) {
        return result.to_string();
      }
      witnessed_sums.push(result);
    }
  }
}

#[cfg(test)]
mod b_tests {
  use super::*;

  #[test]
  fn first_test() {
    let res: String = b("+1\n-1");
    assert_eq!(res, "0");
  }

  #[test]
  fn second_test() {
    let res: String = b("+3\n+3\n+4\n-2\n-4");
    assert_eq!(res, "10");
  }

  #[test]
  fn third_test() {
    let res: String = b("-6\n+3\n+8\n+5\n-6");
    assert_eq!(res, "5");
  }

  #[test]
  fn fourth_test() {
    let res: String = b("+7\n+7\n-2\n-7\n-4");
    assert_eq!(res, "14");
  }
}
