module.exports = rawClaims => {
  const claims = rawClaims.map(parseClaim)
  const blanket = []
  let count = 0

  for (let claim of claims) {

    for (let i=claim.col; i < claim.col + claim.width; i++) {
      for (let j=claim.row; j < claim.row + claim.height; j++) {
        if (!blanket[i]) {
          blanket[i] = []
        }

        blanket[i][j] = blanket[i][j] || 0
        blanket[i][j]++
      }
    }
  }

  for (let claim of claims) {
    let hasIntersection = false

    for (let i=claim.col; i < claim.col + claim.width; i++) {
      for (let j=claim.row; j < claim.row + claim.height; j++) {
        if(blanket[i][j] != 1)
          hasIntersection = true
      }
    }

    if(!hasIntersection) return claim.id
  }
}

function parseClaim(str) {
  const matches = str.match(/#([\d]+) @ ([\d]+),([\d]+): ([\d]+)x([\d]+)$/)  
  return {
    id: parseInt(matches[1]),
    col: parseInt(matches[2]),
    row: parseInt(matches[3]),
    width: parseInt(matches[4]),
    height: parseInt(matches[5]),
  }
}
