const solution = require(require('path').resolve(process.argv[2]))
const input = require('fs').readFileSync(process.argv[3]).toString().split('\n').filter(Boolean)

console.log('Solution:', solution(input))
