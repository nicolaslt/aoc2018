const file = require('fs').readFileSync(process.argv[2])
const operations = file.toString()
	.split("\n")
	.filter(Boolean)
	.map(str => parseInt(str))

const history = [0]
let value = 0
while (true) {
	console.log('loop', value)
	for (let operation of operations) {
		value += operation
		if (history.includes(value)) {
			return console.log(value)
		}
		history.push(value)
	}
}
