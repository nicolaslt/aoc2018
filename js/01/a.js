module.exports = operations => operations
	.map(str => parseInt(str))
	.reduce((value, operation) => value + operation, 0)
