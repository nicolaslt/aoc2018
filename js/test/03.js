const { expect } = require('chai')
const solver = require('../03/a')

describe('day 3, part a', () => {
  it('is able to solve the example', () => {
    const result = solver(["#1 @ 1,3: 4x4","#2 @ 3,1: 4x4","#3 @ 5,5: 2x2"])
    expect(result).to.equal(4)
  })
})
