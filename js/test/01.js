const { expect } = require('chai')
const solver = require('../01/a')

describe('day 1', () => {
	it('should make a sum', () => {
		const result = solver(['+1','+2','+3'])
		expect(result).to.equal(6)
	})

	it('supports negative numbers', () => {
		const result = solver(['+1','+2','-1', '+3'])
		expect(result).to.equal(5)
	})
})
