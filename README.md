# Advent of code 2018

This is my first attempt at [Advent Of Code](https://adventofcode.com/2018/).

I ended up giving up pretty early, needing to focus my thoughts on work instead.

I might pick it up again though, it was fun!

Here you'll find:
- Horrendous looking `rust` code. This was my first contact and I was definitely using the wrong data structures for the job.
- Horrendous looking `javascript` code, that just solves the puzzle. I wrote those usually to validate my understanding of the problem.
